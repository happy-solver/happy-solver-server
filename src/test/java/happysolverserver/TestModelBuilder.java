package happysolverserver;

import static java.util.Arrays.asList;

import java.util.List;

import happysolverserver.persistence.BinPackingItem;
import happysolverserver.persistence.BinPackingModel;

public class TestModelBuilder {

	public static BinPackingModel buildModelRest() {

		BinPackingItem itemRest = new BinPackingItem();
		itemRest.setAmount(3);
		itemRest.setName("1");

		BinPackingItem itemRest2 = new BinPackingItem();
		itemRest2.setAmount(3);
		itemRest2.setName("2");

		List<BinPackingItem> items = asList(itemRest, itemRest2);

		BinPackingModel modelRest = new BinPackingModel();
		modelRest.setName("MyFirstModel");
		modelRest.setId(386L);
		modelRest.setCapacity(10);
		modelRest.setItems(items);

		return modelRest;
	}

}
