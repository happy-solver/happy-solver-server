package happysolverserver.config;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Happy Solver Server", description = "This page documents RESTful "
		+ "Web Service endpoints of the Happy Solver Server which is a "
		+ "implementation of some optimization algorythms."))
public class OpenAPIConfig {
	// see
	// https://raymondhlee.wordpress.com/2019/11/19/adding-openapi-specifications-to-spring-boot-restful-apis/
}