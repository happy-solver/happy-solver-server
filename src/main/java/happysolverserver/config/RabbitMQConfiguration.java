package happysolverserver.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class RabbitMQConfiguration {

	@Value("${spring.rabbitmq.model-queue}")
	private String modelQueue;

	@Bean
	public Queue modelQueue() {
		return new Queue(modelQueue);
	}
}
