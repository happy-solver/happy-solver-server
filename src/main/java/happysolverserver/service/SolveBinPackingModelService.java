package happysolverserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import happysolverserver.persistence.BinPackingModel;
import lombok.extern.flogger.Flogger;

@Flogger
@Service
public class SolveBinPackingModelService {

	@Autowired
	private BinPackingModelService modelService;

	@Autowired
	private RabbitMQClient rabbitMQClient;

	@Autowired
	private AgentService agentService;

	public String solveModel(Long id) {
		if (id == null || id < 0) {
			String message = "Invalid model-id: " + id;
			log.atWarning().log(message);
			return message;
		}

		BinPackingModel model = modelService.findById(id);
		if (model == null) {
			String message = "No model found with id: " + id;
			log.atWarning().log(message);
			return message;
		}

		if (!agentService.isAgentRegistered()) {
			String message = "No agent registered, will not solve model with id: " + id;
			log.atWarning().log(message);
			return message;
		}

		rabbitMQClient.sendModel(model);

		String message = "Send model to agent with id: " + id;
		log.atInfo().log(message);
		return message;
	}
}
