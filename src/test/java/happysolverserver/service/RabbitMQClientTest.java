package happysolverserver.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.RabbitMQContainer;

import happysolverserver.TestModelBuilder;
import happysolverserver.persistence.BinPackingModel;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RabbitMQClientTest {

	@ClassRule
	public static RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3.7-management")
			.withExposedPorts(5672).withVhost("/");

	@BeforeClass
	public static void setupClass() {
		rabbitMQContainer.start();

		String propertyHost = "spring.rabbitmq.host";
		String host = rabbitMQContainer.getContainerIpAddress();
		System.setProperty(propertyHost, host);

		String propertyPort = "spring.rabbitmq.port";
		String port = rabbitMQContainer.getFirstMappedPort() + "";
		System.setProperty(propertyPort, port);
	}

	@Autowired
	private RabbitMQClient client;

	@Test
	public void send_and_receive_model() throws Exception {

		BinPackingModel model = TestModelBuilder.buildModelRest();
		client.sendModel(model);

		Thread.sleep(1_000);

		BinPackingModel receiveModel = client.receiveModel();

		assertThat(receiveModel).isNotNull();
		assertThat(receiveModel.getId()).isEqualTo(model.getId());
		assertThat(receiveModel.getName()).isEqualTo(model.getName());
	}
}