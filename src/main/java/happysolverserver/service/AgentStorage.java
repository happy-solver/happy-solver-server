package happysolverserver.service;

import java.time.Instant;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import happysolverserver.controller.resources.AgentRegister;
import lombok.extern.flogger.Flogger;

@Flogger
public class AgentStorage {

	// last registered time
	private final Map<AgentRegister, Instant> agentRegisterTimeMap = new LinkedHashMap<>();

	public Set<AgentRegister> getRegisteredAgents() {
		return Collections.unmodifiableSet(agentRegisterTimeMap.keySet());
	}

	public Instant getRegisteredTime(AgentRegister agent) {
		return agentRegisterTimeMap.get(agent);
	}

	public void registerAgent(AgentRegister agent) {
		log.atInfo().log("Register agent: " + agent.getAgentName());
		agentRegisterTimeMap.put(agent, Instant.now());
	}

	public void removeAgents(Set<AgentRegister> outdatedAgents) {
		outdatedAgents.stream().forEach(agent -> {
			log.atInfo().log("Remove agent: " + agent.getAgentName());
			agentRegisterTimeMap.remove(agent);
		});
	}
}
