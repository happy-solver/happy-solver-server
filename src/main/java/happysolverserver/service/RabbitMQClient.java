package happysolverserver.service;

import static org.apache.commons.lang3.StringUtils.isBlank;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import happysolverserver.config.RabbitMQConfiguration;
import happysolverserver.persistence.BinPackingModel;

@Component
public class RabbitMQClient {

	@Autowired
	private RabbitMQConfiguration configuration;

	@Autowired
	private RabbitTemplate template;

	public void sendModel(BinPackingModel model) {
		sendJson(configuration.getModelQueue(), model);
	}

	private void sendJson(String queueName, Object toSend) {
		String json = JsonSerializer.toJson(toSend, false);
		template.convertAndSend(queueName, json);
	}

	public BinPackingModel receiveModel() {
		String modelString = (String) template.receiveAndConvert(configuration.getModelQueue());
		if (isBlank(modelString)) {
			return null;
		}
		return JsonSerializer.fromJson(modelString, BinPackingModel.class);
	}
}
