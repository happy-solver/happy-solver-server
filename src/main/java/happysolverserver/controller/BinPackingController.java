package happysolverserver.controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import happysolverserver.persistence.BinPackingModel;
import happysolverserver.service.BinPackingModelService;
import happysolverserver.service.ReceiveBinPackingModelService;
import happysolverserver.service.SolveBinPackingModelService;
import lombok.extern.flogger.Flogger;

@Flogger
@RestController
@RequestMapping(BinPackingController.MAPPING)
public class BinPackingController {

	public static final String MAPPING = "/binpacking";
	public static final String MODELS = "/models";
	public static final String ADD = "/add";
	public static final String SOLVE = "/solve";

	@Autowired
	@Qualifier("rest-thread-pool")
	private ThreadPoolTaskScheduler threadPool;

	@Autowired
	private BinPackingModelService modelService;

	@Autowired
	private ReceiveBinPackingModelService receiveModelService;

	@Autowired
	private SolveBinPackingModelService solveModelService;

	@GetMapping(MODELS)
	public CompletableFuture<List<BinPackingModel>> getAllModels() {
		log.atInfo().log("Get all models");
		return CompletableFuture.supplyAsync(modelService::findAll, threadPool);
	}

	@GetMapping(MODELS + "/{id}")
	public CompletableFuture<BinPackingModel> getModel(@PathVariable("id") Long id) {
		log.atInfo().log("Get model " + id);
		return CompletableFuture.supplyAsync(() -> modelService.findById(id), threadPool);
	}

	@PostMapping(MODELS + ADD)
	public CompletableFuture<String> addModel(@RequestBody BinPackingModel model) {
		log.atInfo().log("Receive model.");
		return CompletableFuture.supplyAsync(() -> receiveModelService.receiveNewModel(model), threadPool);
	}

	@PostMapping(MODELS + SOLVE)
	public CompletableFuture<String> solveModel(@PathVariable("id") Long id) {
		log.atInfo().log("Solve model: " + id);
		return CompletableFuture.supplyAsync(() -> solveModelService.solveModel(id), threadPool);
	}
}
